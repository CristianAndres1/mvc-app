﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JokesApp.Models
{
    public class Joke //Clase pricipal
    {
        public int id { get; set; }//propiedades de la clase (Atributos con accesador y mutador)
        public string jokeQuestion { get; set; }//shorcut: prop + tab + tab
        public string jokeAnswer { get; set; }

        //shorcut: ctor + tab + tab
        public Joke()//constructor vacio
        {
                
        }
    }
}
